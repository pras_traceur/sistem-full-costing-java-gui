-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Oct 20, 2016 at 12:31 AM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `ud_sena`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
`no` int(10) NOT NULL,
  `user` varchar(10) NOT NULL,
  `pass` varchar(10) NOT NULL,
  `posisi` varchar(20) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`no`, `user`, `pass`, `posisi`) VALUES
(1, 'sena', 'sena', 'operator'),
(3, 'admin', 'admin', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `bahan`
--

CREATE TABLE IF NOT EXISTS `bahan` (
`no` int(11) NOT NULL,
  `jenis` varchar(30) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `satuan` varchar(30) NOT NULL,
  `nama_barang` varchar(30) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bahan`
--

INSERT INTO `bahan` (`no`, `jenis`, `jumlah`, `satuan`, `nama_barang`) VALUES
(6, 'kayu', 250, 'desimeter kubik', 'kursi'),
(7, 'paku', 2, 'kg', 'kursi'),
(8, 'kayu', 250, 'desimeter kubik', 'kursi_sudut');

-- --------------------------------------------------------

--
-- Table structure for table `barang_produksi`
--

CREATE TABLE IF NOT EXISTS `barang_produksi` (
  `id_barang` varchar(11) NOT NULL,
  `nama_barang` varchar(30) NOT NULL,
  `persentase_untung` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `barang_produksi`
--

INSERT INTO `barang_produksi` (`id_barang`, `nama_barang`, `persentase_untung`) VALUES
('br_1', 'kursi_sudut', 0),
('br_2', 'kursi', 10);

-- --------------------------------------------------------

--
-- Table structure for table `gaji`
--

CREATE TABLE IF NOT EXISTS `gaji` (
`no` int(11) NOT NULL,
  `tim_karyawan` varchar(30) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `barang_proses` varchar(30) NOT NULL,
  `tgl_proses` date NOT NULL,
  `gaji_karyawan` int(11) NOT NULL,
  `status_gaji` varchar(10) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gaji`
--

INSERT INTO `gaji` (`no`, `tim_karyawan`, `jumlah`, `barang_proses`, `tgl_proses`, `gaji_karyawan`, `status_gaji`) VALUES
(1, 'Tim B', 2, 'kursi', '2016-08-15', 400000, 'belum'),
(4, 'Tim B', 3, 'kursi', '2016-08-17', 1500000, 'belum');

-- --------------------------------------------------------

--
-- Table structure for table `karyawan`
--

CREATE TABLE IF NOT EXISTS `karyawan` (
  `id_karyawan` varchar(10) NOT NULL,
  `tim_karyawan` varchar(30) NOT NULL,
  `gaji_karyawan` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `karyawan`
--

INSERT INTO `karyawan` (`id_karyawan`, `tim_karyawan`, `gaji_karyawan`) VALUES
('a101', 'Tim A', 550000),
('a102', 'Tim B', 500000);

-- --------------------------------------------------------

--
-- Table structure for table `mesin`
--

CREATE TABLE IF NOT EXISTS `mesin` (
  `id_mesin` varchar(10) NOT NULL,
  `nama_mesin` varchar(30) NOT NULL,
  `jumlah_mesin` int(11) NOT NULL,
  `fungsi_mesin` varchar(30) NOT NULL,
  `perawatan_mesin` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mesin`
--

INSERT INTO `mesin` (`id_mesin`, `nama_mesin`, `jumlah_mesin`, `fungsi_mesin`, `perawatan_mesin`) VALUES
('ms_1', 'Serut Kayu', 2, 'Menghaluskan Kayu', 1000);

-- --------------------------------------------------------

--
-- Table structure for table `perawatan_mesin`
--

CREATE TABLE IF NOT EXISTS `perawatan_mesin` (
`no` int(11) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `barang_proses` varchar(30) NOT NULL,
  `tgl_proses` date NOT NULL,
  `biaya_perawatan` int(11) NOT NULL,
  `status_biayaperawatan` varchar(10) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `perawatan_mesin`
--

INSERT INTO `perawatan_mesin` (`no`, `jumlah`, `barang_proses`, `tgl_proses`, `biaya_perawatan`, `status_biayaperawatan`) VALUES
(1, 2, 'kursi', '2016-08-15', 4000, 'belum'),
(4, 3, 'kursi', '2016-08-17', 6000, 'belum');

-- --------------------------------------------------------

--
-- Table structure for table `proses`
--

CREATE TABLE IF NOT EXISTS `proses` (
`no` int(11) NOT NULL,
  `pemesan` varchar(30) NOT NULL,
  `hp` varchar(30) NOT NULL,
  `tgl_proses` date NOT NULL,
  `tim_karyawan` varchar(30) NOT NULL,
  `barang_proses` varchar(30) NOT NULL,
  `harga_mentah` int(11) NOT NULL,
  `persentase_untung` int(11) NOT NULL,
  `untung_perproduk` int(11) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `untung_seluruh` int(11) NOT NULL,
  `harga_jual` int(11) NOT NULL,
  `total` int(11) NOT NULL,
  `status` varchar(30) NOT NULL,
  `perawatan_mesin` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `proses`
--

INSERT INTO `proses` (`no`, `pemesan`, `hp`, `tgl_proses`, `tim_karyawan`, `barang_proses`, `harga_mentah`, `persentase_untung`, `untung_perproduk`, `jumlah`, `untung_seluruh`, `harga_jual`, `total`, `status`, `perawatan_mesin`) VALUES
(1, 'sena', '52346457', '2016-08-15', 'Tim B', 'kursi', 422000, 10, 20000, 2, 40000, 220000, 844000, 'belum', 4000),
(2, 'sena', '083833003407', '2016-08-17', 'Tim B', 'kursi', 986000, 10, 44000, 2, 88000, 484000, 1972000, 'belum', 4000);

-- --------------------------------------------------------

--
-- Table structure for table `stok`
--

CREATE TABLE IF NOT EXISTS `stok` (
`no` int(11) NOT NULL,
  `nama_bahan` varchar(30) NOT NULL,
  `jenis` varchar(30) NOT NULL,
  `harga` int(11) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `satuan` varchar(30) NOT NULL,
  `tgl_beli` date NOT NULL,
  `supplayer` varchar(30) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `stok`
--

INSERT INTO `stok` (`no`, `nama_bahan`, `jenis`, `harga`, `jumlah`, `satuan`, `tgl_beli`, `supplayer`) VALUES
(1, 'jati', 'kayu', 1600, 8750, 'desimeter kubik', '2016-08-15', 'sena'),
(2, 'paku A', 'paku', 20000, 990, 'kg', '2016-08-04', 'sena'),
(3, 'lem A', 'lem kayu', 35000, 1000, 'kg', '2016-08-15', 'sena');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
 ADD PRIMARY KEY (`no`);

--
-- Indexes for table `bahan`
--
ALTER TABLE `bahan`
 ADD PRIMARY KEY (`no`);

--
-- Indexes for table `barang_produksi`
--
ALTER TABLE `barang_produksi`
 ADD PRIMARY KEY (`id_barang`);

--
-- Indexes for table `gaji`
--
ALTER TABLE `gaji`
 ADD PRIMARY KEY (`no`);

--
-- Indexes for table `karyawan`
--
ALTER TABLE `karyawan`
 ADD PRIMARY KEY (`id_karyawan`);

--
-- Indexes for table `mesin`
--
ALTER TABLE `mesin`
 ADD PRIMARY KEY (`id_mesin`);

--
-- Indexes for table `perawatan_mesin`
--
ALTER TABLE `perawatan_mesin`
 ADD PRIMARY KEY (`no`);

--
-- Indexes for table `proses`
--
ALTER TABLE `proses`
 ADD PRIMARY KEY (`no`);

--
-- Indexes for table `stok`
--
ALTER TABLE `stok`
 ADD PRIMARY KEY (`no`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
MODIFY `no` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `bahan`
--
ALTER TABLE `bahan`
MODIFY `no` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `gaji`
--
ALTER TABLE `gaji`
MODIFY `no` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `perawatan_mesin`
--
ALTER TABLE `perawatan_mesin`
MODIFY `no` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `proses`
--
ALTER TABLE `proses`
MODIFY `no` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `stok`
--
ALTER TABLE `stok`
MODIFY `no` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
