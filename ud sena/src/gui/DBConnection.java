/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import javax.swing.JOptionPane;
 
public class DBConnection {
    private Connection conn = null;
    static String DB_URL , DB_USER , DB_PASS , dbname,host,port;
    boolean koneksi = false, db = false;
    public DBConnection() {
 
            
        if (conn == null) {
            try {
                Class.forName("com.mysql.jdbc.Driver");
                conn = DriverManager.getConnection("jdbc:mysql://localhost/ud_sena","root","");
                koneksi = true;
            } catch (SQLException ex) {
                   JOptionPane.showMessageDialog(null, ex.getErrorCode() + ": "+ex.getMessage());
                   koneksi = false;
            } catch (ClassNotFoundException cne) {
                JOptionPane.showMessageDialog(null, "COM.MYSQL.JDBC.Driver tidak ditemukan");
                koneksi = false;
            }
        }
    }  
 
    public Connection getCon() throws SQLException {
        return this.conn;
    }
 
    public boolean getKoneksi() {
        return koneksi;
    }
}
