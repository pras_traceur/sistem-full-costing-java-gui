/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

import java.sql.*;
import java.util.HashMap;
import java.util.Map;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.view.JasperViewer;

/**
 *
 * @author SENA
 */
public class admin_laporan extends javax.swing.JFrame {
Connection koneksi;
ResultSet RsBrg,RsBrg1,RsBrg2;
Statement stm,stm1,stm2;
String combobarang;
String tim,jumlah,barang,tanggal;

private Object[][] dataTable = null;
private String[] header = {"no","pemesan","hp","tanggal","tukang","proses","untung %","jumlah","untung bersih","total","status"};
private Object[][] dataTable1 = null;
private String[] header1 = {"no","tim","jumlah","barang","tanggal","gaji","Bayar"};
private Object[][] dataTable2 = null;
private String[] header2 = {"no","jumlah","barang","tanggal","biaya perawatan","Bayar"};

    /**
     * Creates new form menu
     */
    public admin_laporan() {
        initComponents();
        posisi();
        buka_db();
        baca_data();
        baca_gaji();
        baca_perawatan();
    }
    

void posisi(){
    Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
    Dimension frameSize = getSize();
    setLocation(
    (screenSize.width - frameSize.width) / 2,
    (screenSize.height - frameSize.height) / 2);
}

private void buka_db(){
    try{
    Class.forName("com.mysql.jdbc.Driver");
    koneksi = (Connection) DriverManager.getConnection("jdbc:mysql://localhost/ud_sena","root","");
    System.out.println("konek");
    }catch(ClassNotFoundException e){
    System.out.println("Eror #1(gak konek) : "+ e.getMessage());
    System.exit(0);
    }catch(SQLException e){
    System.out.println("Eror #2(gak konek) : "+ e.getMessage());
    System.exit(0);
}
} 

private void baca_data(){    
try{

stm = koneksi.createStatement();
//WHERE (tanggal BETWEEN '2007-12-01' AND '2008-01-01')
RsBrg = stm.executeQuery("SELECT no,pemesan,hp,tgl_proses,tim_karyawan,barang_proses,persentase_untung,jumlah,untung_seluruh,total,status FROM proses");

ResultSetMetaData meta = RsBrg.getMetaData();
int col = meta.getColumnCount();
int baris = 0;
while(RsBrg.next()) {
baris = RsBrg.getRow();
}
dataTable = new Object[baris][col];
int x = 0;
RsBrg.beforeFirst();
while(RsBrg.next()) {
dataTable[x][0] = RsBrg.getString("no");
dataTable[x][1] = RsBrg.getString("pemesan");
dataTable[x][2] = RsBrg.getString("hp");
dataTable[x][3] = RsBrg.getString("tgl_proses");
dataTable[x][4] = RsBrg.getString("tim_karyawan");
dataTable[x][5] = RsBrg.getString("barang_proses");
dataTable[x][6] = RsBrg.getString("persentase_untung");
dataTable[x][7] = RsBrg.getString("jumlah");
dataTable[x][8] = RsBrg.getString("untung_seluruh");
dataTable[x][9] = RsBrg.getString("total");
dataTable[x][10] = RsBrg.getString("status");
x++;
    
}
in_laporan.setModel(new DefaultTableModel(dataTable,header));
}catch(SQLException e){
JOptionPane.showMessageDialog(null, "gagal memuat database");

    }

}  

private void baca_gaji(){    
try{

stm1 = koneksi.createStatement();
//WHERE (tanggal BETWEEN '2007-12-01' AND '2008-01-01')
RsBrg1 = stm1.executeQuery("SELECT * FROM gaji");

ResultSetMetaData meta = RsBrg1.getMetaData();
int col = meta.getColumnCount();
int baris = 0;
while(RsBrg1.next()) {
baris = RsBrg1.getRow();
}
dataTable1 = new Object[baris][col];
int x = 0;
RsBrg1.beforeFirst();
while(RsBrg1.next()) {
dataTable1[x][0] = RsBrg1.getString("no");
dataTable1[x][1] = RsBrg1.getString("tim_karyawan");
dataTable1[x][2] = RsBrg1.getString("jumlah");
dataTable1[x][3] = RsBrg1.getString("barang_proses");
dataTable1[x][4] = RsBrg1.getString("tgl_proses");
dataTable1[x][5] = RsBrg1.getString("gaji_karyawan");
dataTable1[x][6] = RsBrg1.getString("status_gaji");
x++;
    
}
laporan_gaji_karyawan.setModel(new DefaultTableModel(dataTable1,header1));
}catch(SQLException e){
JOptionPane.showMessageDialog(null, "gagal memuat database");

    }

}

private void baca_perawatan(){    
try{

stm2 = koneksi.createStatement();
//WHERE (tanggal BETWEEN '2007-12-01' AND '2008-01-01')
RsBrg2 = stm2.executeQuery("SELECT * FROM perawatan_mesin");

ResultSetMetaData meta = RsBrg2.getMetaData();
int col = meta.getColumnCount();
int baris = 0;
while(RsBrg2.next()) {
baris = RsBrg2.getRow();
}
dataTable2 = new Object[baris][col];
int x = 0;
RsBrg2.beforeFirst();
while(RsBrg2.next()) {
dataTable2[x][0] = RsBrg2.getString("no");
dataTable2[x][1] = RsBrg2.getString("jumlah");
dataTable2[x][2] = RsBrg2.getString("barang_proses");
dataTable2[x][3] = RsBrg2.getString("tgl_proses");
dataTable2[x][4] = RsBrg2.getString("biaya_perawatan");
dataTable2[x][5] = RsBrg2.getString("status_biayaperawatan");
x++;
    
}
laporan_perawatan_mesin.setModel(new DefaultTableModel(dataTable2,header2));
}catch(SQLException e){
JOptionPane.showMessageDialog(null, "gagal memuat database");

    }

}

public void field(){
    int row=in_laporan.getSelectedRow();
    pr_no.setText((String)in_laporan.getValueAt(row,0));
    tim = ((String)in_laporan.getValueAt(row,4));
    jumlah=((String)in_laporan.getValueAt(row,7));
    barang =((String)in_laporan.getValueAt(row,5));
    tanggal=((String)in_laporan.getValueAt(row,3));
    pr_status.setSelectedItem((String)in_laporan.getValueAt(row,10));
   // 4753
            
}

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        bgadmin1 = new source_gambar.bgadmin();
        home = new javax.swing.JButton();
        input = new javax.swing.JButton();
        produk = new javax.swing.JButton();
        stok = new javax.swing.JButton();
        karyawan = new javax.swing.JButton();
        laporan = new javax.swing.JButton();
        lain = new javax.swing.JButton();
        logout = new javax.swing.JButton();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel1 = new javax.swing.JPanel();
        pr_no = new javax.swing.JTextField();
        btn_laporan = new javax.swing.JButton();
        simpan = new javax.swing.JButton();
        pr_status = new javax.swing.JComboBox();
        jLabel12 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        in_laporan = new javax.swing.JTable();
        hapus = new javax.swing.JButton();
        jLabel11 = new javax.swing.JLabel();
        btn_kwitansi = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        laporan_gaji_karyawan = new javax.swing.JTable();
        jButton1 = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        jScrollPane3 = new javax.swing.JScrollPane();
        laporan_perawatan_mesin = new javax.swing.JTable();
        jButton2 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setUndecorated(true);

        bgadmin1.setPreferredSize(new java.awt.Dimension(1036, 600));

        home.setIcon(new javax.swing.ImageIcon(getClass().getResource("/gambar/btnhome.png"))); // NOI18N
        home.setText(" ");
        home.setPreferredSize(new java.awt.Dimension(260, 60));
        home.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                homeActionPerformed(evt);
            }
        });

        input.setIcon(new javax.swing.ImageIcon(getClass().getResource("/gambar/btninput.png"))); // NOI18N
        input.setText(" ");
        input.setPreferredSize(new java.awt.Dimension(260, 60));
        input.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                inputActionPerformed(evt);
            }
        });

        produk.setIcon(new javax.swing.ImageIcon(getClass().getResource("/gambar/btnproduk.png"))); // NOI18N
        produk.setText(" ");
        produk.setPreferredSize(new java.awt.Dimension(260, 60));
        produk.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                produkActionPerformed(evt);
            }
        });

        stok.setIcon(new javax.swing.ImageIcon(getClass().getResource("/gambar/btnstok.png"))); // NOI18N
        stok.setText(" ");
        stok.setPreferredSize(new java.awt.Dimension(260, 60));
        stok.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                stokActionPerformed(evt);
            }
        });

        karyawan.setIcon(new javax.swing.ImageIcon(getClass().getResource("/gambar/btnkaryawan.png"))); // NOI18N
        karyawan.setText(" ");
        karyawan.setPreferredSize(new java.awt.Dimension(260, 60));
        karyawan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                karyawanActionPerformed(evt);
            }
        });

        laporan.setIcon(new javax.swing.ImageIcon(getClass().getResource("/gambar/btnlaporan1.png"))); // NOI18N
        laporan.setText(" ");
        laporan.setPreferredSize(new java.awt.Dimension(260, 60));

        lain.setIcon(new javax.swing.ImageIcon(getClass().getResource("/gambar/btnalat.png"))); // NOI18N
        lain.setText(" ");
        lain.setPreferredSize(new java.awt.Dimension(260, 60));
        lain.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                lainActionPerformed(evt);
            }
        });

        logout.setIcon(new javax.swing.ImageIcon(getClass().getResource("/gambar/btnlogout.png"))); // NOI18N
        logout.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                logoutActionPerformed(evt);
            }
        });

        btn_laporan.setText("laporan");
        btn_laporan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_laporanActionPerformed(evt);
            }
        });

        simpan.setText("simpan");
        simpan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                simpanActionPerformed(evt);
            }
        });

        pr_status.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "------------", "sudah", "belum" }));

        jLabel12.setText("No_Pesanan");

        in_laporan.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null}
            },
            new String [] {
                "1", "2", "3", "4", "5", "6", "7", "8", "Title 9", "Title 10"
            }
        ));
        in_laporan.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                in_laporanMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(in_laporan);

        hapus.setText("hapus");
        hapus.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                hapusActionPerformed(evt);
            }
        });

        jLabel11.setText("Proses");

        btn_kwitansi.setText("kwitansi");
        btn_kwitansi.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_kwitansiActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 675, Short.MAX_VALUE)
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 660, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addGap(9, 9, 9)
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jLabel11)
                                .addComponent(jLabel12))
                            .addGap(39, 39, 39)
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(pr_status, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(pr_no, javax.swing.GroupLayout.PREFERRED_SIZE, 73, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGap(36, 36, 36)
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(simpan, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(hapus, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGap(35, 35, 35)
                            .addComponent(btn_kwitansi, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGap(33, 33, 33)
                            .addComponent(btn_laporan, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 404, Short.MAX_VALUE)
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel1Layout.createSequentialGroup()
                    .addGap(25, 25, 25)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 259, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(43, 43, 43)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabel12)
                                .addComponent(pr_no, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(simpan))
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(pr_status, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabel11)
                                .addComponent(hapus)))
                        .addComponent(btn_kwitansi, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btn_laporan, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addContainerGap(25, Short.MAX_VALUE)))
        );

        jTabbedPane1.addTab("Laporan Proses Produksi", jPanel1);

        laporan_gaji_karyawan.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null}
            },
            new String [] {
                "no", "bahan", "jumlah", "satuan", "Title 5", "Title 6", "Title 7"
            }
        ));
        jScrollPane2.setViewportView(laporan_gaji_karyawan);
        if (laporan_gaji_karyawan.getColumnModel().getColumnCount() > 0) {
            laporan_gaji_karyawan.getColumnModel().getColumn(6).setHeaderValue("Title 7");
        }

        jButton1.setText("Cetak");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap(55, Short.MAX_VALUE)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 567, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(49, 49, 49))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                        .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(253, 253, 253))))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(56, 56, 56)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 201, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(37, 37, 37)
                .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 49, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(61, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("Laporan Gaji Karyawan", jPanel2);

        laporan_perawatan_mesin.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null}
            },
            new String [] {
                "no", "bahan", "jumlah", "satuan", "Title 5", "Title 6"
            }
        ));
        jScrollPane3.setViewportView(laporan_perawatan_mesin);

        jButton2.setText("Cetak");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap(55, Short.MAX_VALUE)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                        .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 567, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(49, 49, 49))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                        .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 97, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(283, 283, 283))))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(56, 56, 56)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 201, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(26, 26, 26)
                .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 49, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(72, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("Laporan Perawatan Alat", jPanel3);

        javax.swing.GroupLayout bgadmin1Layout = new javax.swing.GroupLayout(bgadmin1);
        bgadmin1.setLayout(bgadmin1Layout);
        bgadmin1Layout.setHorizontalGroup(
            bgadmin1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(bgadmin1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(bgadmin1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(produk, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(stok, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lain, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(laporan, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(karyawan, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(home, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(input, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(41, 41, 41)
                .addComponent(jTabbedPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 676, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(28, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, bgadmin1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(logout, javax.swing.GroupLayout.PREFERRED_SIZE, 86, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(19, 19, 19))
        );
        bgadmin1Layout.setVerticalGroup(
            bgadmin1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(bgadmin1Layout.createSequentialGroup()
                .addGap(24, 24, 24)
                .addComponent(logout)
                .addGap(36, 36, 36)
                .addGroup(bgadmin1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(bgadmin1Layout.createSequentialGroup()
                        .addComponent(home, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(input, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(produk, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(stok, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(karyawan, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(laporan, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(lain, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(jTabbedPane1))
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(bgadmin1, javax.swing.GroupLayout.DEFAULT_SIZE, 955, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(bgadmin1, javax.swing.GroupLayout.PREFERRED_SIZE, 540, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void logoutActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_logoutActionPerformed
        // TODO add your handling code here:
                    login go = new login();
                    go.setVisible(true);
                    dispose();
    }//GEN-LAST:event_logoutActionPerformed

    private void homeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_homeActionPerformed
        // TODO add your handling code here:
                    admin go = new admin();
                    go.setVisible(true);
                    dispose();
    }//GEN-LAST:event_homeActionPerformed

    private void inputActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_inputActionPerformed
        // TODO add your handling code here:
                    admin_input go = new admin_input();
                    go.setVisible(true);
                    dispose();
    }//GEN-LAST:event_inputActionPerformed

    private void produkActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_produkActionPerformed
        // TODO add your handling code here:
                    admin_produk go = new admin_produk();
                    go.setVisible(true);
                    dispose();
    }//GEN-LAST:event_produkActionPerformed

    private void stokActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_stokActionPerformed
        // TODO add your handling code here:
                    admin_stok go = new admin_stok();
                    go.setVisible(true);
                    dispose();
    }//GEN-LAST:event_stokActionPerformed

    private void karyawanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_karyawanActionPerformed
        // TODO add your handling code here:
                    admin_karyawan go = new admin_karyawan();
                    go.setVisible(true);
                    dispose();
    }//GEN-LAST:event_karyawanActionPerformed

    private void lainActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_lainActionPerformed
        // TODO add your handling code here:
                    admin_lain go = new admin_lain();
                    go.setVisible(true);
                    dispose();
    }//GEN-LAST:event_lainActionPerformed

    private void btn_laporanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_laporanActionPerformed
        // TODO add your handling code here:
        DBConnection con=new DBConnection();
        String currentDir = System.getProperty("user.dir");
        java.io.File namaFile=new java.io.File(currentDir+"/laporanadmin.jasper");
        try {
            net.sf.jasperreports.engine.JasperReport jasper;
            jasper=(net.sf.jasperreports.engine.JasperReport)
            net.sf.jasperreports.engine.util.JRLoader.loadObject(namaFile.getPath());
            net.sf.jasperreports.engine.JasperPrint jp;
            jp=net.sf.jasperreports.engine.JasperFillManager.fillReport(jasper, null,con.getCon());
            net.sf.jasperreports.view.JasperViewer.viewReport(jp,false);
        } catch (Exception ex) {
            javax.swing.JOptionPane.showMessageDialog(rootPane, ex.getMessage());
        }
    }//GEN-LAST:event_btn_laporanActionPerformed

    private void simpanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_simpanActionPerformed
        // TODO add your handling code here:
        try{
            String pr = (String)pr_status.getSelectedItem();
            stm.executeUpdate("update proses set status='"+pr+"' where no='" +pr_no.getText() + "'") ;

        }catch(SQLException e) {}
        
try{
            String pr = (String)pr_status.getSelectedItem();
            stm.executeUpdate("update gaji set status_gaji='"+pr+"' where no='"+pr_no.getText()+"'") ;

        }catch(SQLException e) {}
         
        try{
            String pr = (String)pr_status.getSelectedItem();
            stm.executeUpdate("update perawatan_mesin set status_biayaperawatan='"+pr+"' where no='"+pr_no.getText()+"'") ;
            JOptionPane.showMessageDialog(null,"Update berhasil");
        }catch(SQLException e) {
            JOptionPane.showMessageDialog(null,"Operasi Gagal");
        }
        
        baca_data();
    }//GEN-LAST:event_simpanActionPerformed

    private void in_laporanMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_in_laporanMouseClicked
        // TODO add your handling code here:
        field();
    }//GEN-LAST:event_in_laporanMouseClicked

    private void hapusActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_hapusActionPerformed
        // TODO add your handling code here:
        try{
            String sql="delete from proses where no='"+pr_no.getText()+"'";
            stm.executeUpdate(sql);
           
        }catch(SQLException e){
          
        }
        try{
            String sql="delete from gaji where no='"+pr_no.getText()+"'";
            stm.executeUpdate(sql);
         
        }catch(SQLException e){
          
        }
        try{
            String sql="delete from perawatan_mesin where no='"+pr_no.getText()+"'";
            stm.executeUpdate(sql);
            JOptionPane.showMessageDialog(null, "delete berhasil");
        }catch(SQLException e){
            JOptionPane.showMessageDialog(null, "delete gagal");
        }
        baca_data();
        baca_gaji();
        baca_perawatan();
    }//GEN-LAST:event_hapusActionPerformed

    private void btn_kwitansiActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_kwitansiActionPerformed
        // TODO add your handling code here:
        Connection con = null;
        try {
            String jdbcDriver = "com.mysql.jdbc.Driver";
            Class.forName(jdbcDriver);

            String url = "jdbc:mysql://localhost/ud_sena"; // database yang dibuat
            String user = "root";   //user dari database
            String pass ="";    //password database

            con = DriverManager.getConnection(url, user, pass);
            Statement stm = (Statement) con.createStatement();

            try {
                String currentDir = System.getProperty("user.dir");

                String path=currentDir+"/kwitansi.jasper";
                Map parameter = new HashMap();
                parameter.put("no",pr_no.getText());
                JasperPrint print = JasperFillManager.fillReport(path,parameter,stm.getConnection());
                JasperViewer.viewReport(print, false);
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(rootPane,"Dokumen Tidak Ada"+ex);
            }

        } catch (Exception e) {
            System.out.println(e);
        }
    }//GEN-LAST:event_btn_kwitansiActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
         // TODO add your handling code here:
        DBConnection con=new DBConnection();
        String currentDir = System.getProperty("user.dir");
        java.io.File namaFile=new java.io.File(currentDir+"/laporangaji.jasper");
        try {
            net.sf.jasperreports.engine.JasperReport jasper;
            jasper=(net.sf.jasperreports.engine.JasperReport)
            net.sf.jasperreports.engine.util.JRLoader.loadObject(namaFile.getPath());
            net.sf.jasperreports.engine.JasperPrint jp;
            jp=net.sf.jasperreports.engine.JasperFillManager.fillReport(jasper, null,con.getCon());
            net.sf.jasperreports.view.JasperViewer.viewReport(jp,false);
        } catch (Exception ex) {
            javax.swing.JOptionPane.showMessageDialog(rootPane, ex.getMessage());
        }
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        // TODO add your handling code here:
                 // TODO add your handling code here:
        DBConnection con=new DBConnection();
        String currentDir = System.getProperty("user.dir");
        java.io.File namaFile=new java.io.File(currentDir+"/laporanperawatan.jasper");
        try {
            net.sf.jasperreports.engine.JasperReport jasper;
            jasper=(net.sf.jasperreports.engine.JasperReport)
            net.sf.jasperreports.engine.util.JRLoader.loadObject(namaFile.getPath());
            net.sf.jasperreports.engine.JasperPrint jp;
            jp=net.sf.jasperreports.engine.JasperFillManager.fillReport(jasper, null,con.getCon());
            net.sf.jasperreports.view.JasperViewer.viewReport(jp,false);
        } catch (Exception ex) {
            javax.swing.JOptionPane.showMessageDialog(rootPane, ex.getMessage());
        }
    }//GEN-LAST:event_jButton2ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(admin_laporan.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(admin_laporan.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(admin_laporan.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(admin_laporan.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new admin_laporan().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private source_gambar.bgadmin bgadmin1;
    private javax.swing.JButton btn_kwitansi;
    private javax.swing.JButton btn_laporan;
    private javax.swing.JButton hapus;
    private javax.swing.JButton home;
    private javax.swing.JTable in_laporan;
    private javax.swing.JButton input;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JButton karyawan;
    private javax.swing.JButton lain;
    private javax.swing.JButton laporan;
    private javax.swing.JTable laporan_gaji_karyawan;
    private javax.swing.JTable laporan_perawatan_mesin;
    private javax.swing.JButton logout;
    private javax.swing.JTextField pr_no;
    private javax.swing.JComboBox pr_status;
    private javax.swing.JButton produk;
    private javax.swing.JButton simpan;
    private javax.swing.JButton stok;
    // End of variables declaration//GEN-END:variables
}
