/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author SENA
 */
public class admin_karyawan extends javax.swing.JFrame {
Connection koneksi;
ResultSet RsBrg;
Statement stm;
private Object[][] dataTable = null;
private String[] header = {"no","Nama","Password","Status"};
private Object[][] dataTable1 = null;
private String[] header1 = {"id Tim","Nama Tim","Gaji per-Produksi"};

    public admin_karyawan() {
        initComponents();
        posisi();
        buka_db();
        karyawan();
        karyawan_produksi();
    }
    
void posisi(){
    Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
    Dimension frameSize = getSize();
    setLocation(
    (screenSize.width - frameSize.width) / 2,
    (screenSize.height - frameSize.height) / 2);
}

private void buka_db(){
    try{
    Class.forName("com.mysql.jdbc.Driver");
    koneksi = (Connection) DriverManager.getConnection("jdbc:mysql://localhost/ud_sena","root","");
    System.out.println("konek");
    }catch(ClassNotFoundException e){
    System.out.println("Eror #1(gak konek) : "+ e.getMessage());
    System.exit(0);
    }catch(SQLException e){
    System.out.println("Eror #2(gak konek) : "+ e.getMessage());
    System.exit(0);
}
}  

private void karyawan(){    
try{

    stm = koneksi.createStatement();
    RsBrg = stm.executeQuery("SELECT * FROM admin");

    ResultSetMetaData meta = RsBrg.getMetaData();
    int col = meta.getColumnCount();
    int baris = 0;
    while(RsBrg.next()) {
    baris = RsBrg.getRow();
    }
    dataTable = new Object[baris][col];
    int x = 0;
    RsBrg.beforeFirst();
    while(RsBrg.next()) {

    dataTable[x][0] = Integer.toString(x+1);
    dataTable[x][1] = RsBrg.getString("user");
    dataTable[x][2] = RsBrg.getString("pass");
    dataTable[x][3] = RsBrg.getString("posisi");
    x++;
    }
    tabel_karyawan.setModel(new DefaultTableModel(dataTable,header));
}catch(SQLException e){
    JOptionPane.showMessageDialog(null, "gagal memuat database");
}

} 

private void karyawan_produksi(){    
try{

    stm = koneksi.createStatement();
    RsBrg = stm.executeQuery("SELECT * FROM karyawan");

    ResultSetMetaData meta = RsBrg.getMetaData();
    int col = meta.getColumnCount();
    int baris = 0;
    while(RsBrg.next()) {
    baris = RsBrg.getRow();
    }
    dataTable1 = new Object[baris][col];
    int x = 0;
    RsBrg.beforeFirst();
    while(RsBrg.next()) {
    dataTable1[x][0] = RsBrg.getString("id_karyawan");
    dataTable1[x][1] = RsBrg.getString("tim_karyawan");
    dataTable1[x][2] = RsBrg.getString("gaji_karyawan");
    x++;
    }
    tabel_karyawan_produksi.setModel(new DefaultTableModel(dataTable1,header1));
}catch(SQLException e){
    JOptionPane.showMessageDialog(null, "gagal memuat database");
}
id_karyawan.setEditable(false);
nama_karyawan.setEditable(false);

} 

public void field(){
    int row=tabel_karyawan.getSelectedRow();
    ko_nama.setText((String)tabel_karyawan.getValueAt(row,1)); 
    ko_status.setSelectedItem((String)tabel_karyawan.getValueAt(row,3)); 
    ko_password.setText((String)tabel_karyawan.getValueAt(row,2));
}

public void field1(){
    int row=tabel_karyawan_produksi.getSelectedRow();
    id_karyawan.setText((String)tabel_karyawan_produksi.getValueAt(row,0)); 
    nama_karyawan.setText((String)tabel_karyawan_produksi.getValueAt(row,1));
    gaji_karyawan.setText((String)tabel_karyawan_produksi.getValueAt(row,2));

}

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        bgadmin1 = new source_gambar.bgadmin();
        home = new javax.swing.JButton();
        input = new javax.swing.JButton();
        produk = new javax.swing.JButton();
        stok = new javax.swing.JButton();
        karyawan = new javax.swing.JButton();
        laporan = new javax.swing.JButton();
        lain = new javax.swing.JButton();
        logout = new javax.swing.JButton();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel1 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        tabel_karyawan_produksi = new javax.swing.JTable();
        jLabel5 = new javax.swing.JLabel();
        id_karyawan = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        nama_karyawan = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        gaji_karyawan = new javax.swing.JTextField();
        edit_karyawan_produksi = new javax.swing.JButton();
        hapus = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tabel_karyawan = new javax.swing.JTable();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        ko_nama = new javax.swing.JTextField();
        ko_password = new javax.swing.JTextField();
        btn_simpan = new javax.swing.JButton();
        btn_hapus = new javax.swing.JButton();
        ko_status = new javax.swing.JComboBox();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setUndecorated(true);

        bgadmin1.setPreferredSize(new java.awt.Dimension(1036, 600));

        home.setIcon(new javax.swing.ImageIcon(getClass().getResource("/gambar/btnhome.png"))); // NOI18N
        home.setText(" ");
        home.setPreferredSize(new java.awt.Dimension(260, 60));
        home.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                homeActionPerformed(evt);
            }
        });

        input.setIcon(new javax.swing.ImageIcon(getClass().getResource("/gambar/btninput.png"))); // NOI18N
        input.setText(" ");
        input.setPreferredSize(new java.awt.Dimension(260, 60));
        input.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                inputActionPerformed(evt);
            }
        });

        produk.setIcon(new javax.swing.ImageIcon(getClass().getResource("/gambar/btnproduk.png"))); // NOI18N
        produk.setText(" ");
        produk.setPreferredSize(new java.awt.Dimension(260, 60));
        produk.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                produkActionPerformed(evt);
            }
        });

        stok.setIcon(new javax.swing.ImageIcon(getClass().getResource("/gambar/btnstok.png"))); // NOI18N
        stok.setText(" ");
        stok.setPreferredSize(new java.awt.Dimension(260, 60));
        stok.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                stokActionPerformed(evt);
            }
        });

        karyawan.setIcon(new javax.swing.ImageIcon(getClass().getResource("/gambar/btnkaryawan1.png"))); // NOI18N
        karyawan.setText(" ");
        karyawan.setPreferredSize(new java.awt.Dimension(260, 60));

        laporan.setIcon(new javax.swing.ImageIcon(getClass().getResource("/gambar/btnlaporan.png"))); // NOI18N
        laporan.setText(" ");
        laporan.setPreferredSize(new java.awt.Dimension(260, 60));
        laporan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                laporanActionPerformed(evt);
            }
        });

        lain.setIcon(new javax.swing.ImageIcon(getClass().getResource("/gambar/btnalat.png"))); // NOI18N
        lain.setText(" ");
        lain.setPreferredSize(new java.awt.Dimension(260, 60));
        lain.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                lainActionPerformed(evt);
            }
        });

        logout.setIcon(new javax.swing.ImageIcon(getClass().getResource("/gambar/btnlogout.png"))); // NOI18N
        logout.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                logoutActionPerformed(evt);
            }
        });

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel4.setText("Setting Gaji Karyawan Produksi");

        tabel_karyawan_produksi.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3"
            }
        ));
        tabel_karyawan_produksi.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tabel_karyawan_produksiMouseClicked(evt);
            }
        });
        jScrollPane3.setViewportView(tabel_karyawan_produksi);

        jLabel5.setText("ID Tim");

        jLabel6.setText("Nama Tim");

        jLabel8.setText("Gaji Perproduksi");

        edit_karyawan_produksi.setText("SIMPAN");
        edit_karyawan_produksi.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                edit_karyawan_produksiActionPerformed(evt);
            }
        });

        hapus.setText("HAPUS");
        hapus.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                hapusActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGap(85, 85, 85)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 465, Short.MAX_VALUE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel5)
                            .addComponent(jLabel6))
                        .addGap(26, 26, 26)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(nama_karyawan, javax.swing.GroupLayout.DEFAULT_SIZE, 142, Short.MAX_VALUE)
                            .addComponent(id_karyawan))
                        .addGap(18, 18, 18)
                        .addComponent(jLabel8)
                        .addGap(18, 18, 18)
                        .addComponent(gaji_karyawan)))
                .addGap(78, 78, 78))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel4)
                .addGap(162, 162, 162))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(227, 227, 227)
                .addComponent(edit_karyawan_produksi, javax.swing.GroupLayout.PREFERRED_SIZE, 83, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(47, 47, 47)
                .addComponent(hapus, javax.swing.GroupLayout.PREFERRED_SIZE, 76, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(26, 26, 26)
                .addComponent(jLabel4)
                .addGap(37, 37, 37)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 91, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 37, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(id_karyawan, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(gaji_karyawan, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel8))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(nama_karyawan, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(38, 38, 38)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(hapus, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(edit_karyawan_produksi, javax.swing.GroupLayout.DEFAULT_SIZE, 31, Short.MAX_VALUE))
                .addContainerGap())
        );

        jTabbedPane1.addTab("Karyawan Produksi", jPanel1);

        tabel_karyawan.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tabel_karyawan.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tabel_karyawanMouseClicked(evt);
            }
        });
        jScrollPane2.setViewportView(tabel_karyawan);

        jLabel1.setText("Nama");

        jLabel2.setText("Status");

        jLabel3.setText("Password");

        btn_simpan.setText("SIMPAN");
        btn_simpan.setMaximumSize(new java.awt.Dimension(50, 20));
        btn_simpan.setMinimumSize(new java.awt.Dimension(50, 20));
        btn_simpan.setPreferredSize(new java.awt.Dimension(50, 20));
        btn_simpan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_simpanActionPerformed(evt);
            }
        });

        btn_hapus.setText("HAPUS");
        btn_hapus.setMaximumSize(new java.awt.Dimension(50, 20));
        btn_hapus.setMinimumSize(new java.awt.Dimension(50, 20));
        btn_hapus.setPreferredSize(new java.awt.Dimension(50, 20));
        btn_hapus.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_hapusActionPerformed(evt);
            }
        });

        ko_status.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "---------------------", "admin", "operator" }));

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(51, 51, 51)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 531, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(46, Short.MAX_VALUE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel1)
                            .addComponent(jLabel2)
                            .addComponent(jLabel3))
                        .addGap(37, 37, 37)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(ko_nama)
                            .addComponent(ko_status, 0, 120, Short.MAX_VALUE)
                            .addComponent(ko_password))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(btn_simpan, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(btn_hapus, javax.swing.GroupLayout.DEFAULT_SIZE, 114, Short.MAX_VALUE))
                        .addGap(75, 75, 75))))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(50, 50, 50)
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 141, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(24, 24, 24)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel1)
                            .addComponent(ko_nama, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(20, 20, 20)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(ko_password, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel3)))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(225, 225, 225)
                        .addComponent(btn_simpan, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(20, 20, 20)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(ko_status, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel2)))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(4, 4, 4)
                        .addComponent(btn_hapus, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(35, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("Karyawan Operator", jPanel2);

        javax.swing.GroupLayout bgadmin1Layout = new javax.swing.GroupLayout(bgadmin1);
        bgadmin1.setLayout(bgadmin1Layout);
        bgadmin1Layout.setHorizontalGroup(
            bgadmin1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(bgadmin1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(bgadmin1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(stok, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lain, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(laporan, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(karyawan, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(home, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(input, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(produk, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(59, 59, 59)
                .addComponent(jTabbedPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 633, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(53, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, bgadmin1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(logout, javax.swing.GroupLayout.PREFERRED_SIZE, 86, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(19, 19, 19))
        );
        bgadmin1Layout.setVerticalGroup(
            bgadmin1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(bgadmin1Layout.createSequentialGroup()
                .addGap(24, 24, 24)
                .addComponent(logout)
                .addGroup(bgadmin1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(bgadmin1Layout.createSequentialGroup()
                        .addGap(36, 36, 36)
                        .addComponent(home, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(input, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(produk, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(stok, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(karyawan, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(laporan, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(lain, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, bgadmin1Layout.createSequentialGroup()
                        .addGap(73, 73, 73)
                        .addComponent(jTabbedPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 379, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(27, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(bgadmin1, javax.swing.GroupLayout.DEFAULT_SIZE, 955, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(bgadmin1, javax.swing.GroupLayout.PREFERRED_SIZE, 540, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void logoutActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_logoutActionPerformed
        // TODO add your handling code here:
                    login go = new login();
                    go.setVisible(true);
                    dispose();
    }//GEN-LAST:event_logoutActionPerformed

    private void homeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_homeActionPerformed
        // TODO add your handling code here:
                    admin go = new admin();
                    go.setVisible(true);
                    dispose();
    }//GEN-LAST:event_homeActionPerformed

    private void inputActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_inputActionPerformed
        // TODO add your handling code here:
                    admin_input go = new admin_input();
                    go.setVisible(true);
                    dispose();
    }//GEN-LAST:event_inputActionPerformed

    private void produkActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_produkActionPerformed
        // TODO add your handling code here:
                    admin_produk go = new admin_produk();
                    go.setVisible(true);
                    dispose();
    }//GEN-LAST:event_produkActionPerformed

    private void stokActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_stokActionPerformed
        // TODO add your handling code here:
                    admin_stok go = new admin_stok();
                    go.setVisible(true);
                    dispose();
    }//GEN-LAST:event_stokActionPerformed

    private void laporanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_laporanActionPerformed
        // TODO add your handling code here:
                    admin_laporan go = new admin_laporan();
                    go.setVisible(true);
                    dispose();
    }//GEN-LAST:event_laporanActionPerformed

    private void lainActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_lainActionPerformed
        // TODO add your handling code here:
                    admin_lain go = new admin_lain();
                    go.setVisible(true);
                    dispose();
    }//GEN-LAST:event_lainActionPerformed

    private void tabel_karyawanMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tabel_karyawanMouseClicked
        // TODO add your handling code here:
        field();
    }//GEN-LAST:event_tabel_karyawanMouseClicked

    private void btn_simpanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_simpanActionPerformed
        // TODO add your handling code here:
         if(ko_nama.getText().equalsIgnoreCase("") || ko_password.getText().equalsIgnoreCase("") || ko_status.getSelectedItem().toString().equalsIgnoreCase("")){
        JOptionPane.showMessageDialog(null,"Maaf lengkapi data Terlebih dahulu");
        }else{
        
        try{
            stm = koneksi.createStatement();
            stm.executeUpdate("insert into admin (user,pass,posisi)value ('"+ko_nama.getText()+"','"+ko_password.getText()+"','"+ko_status.getSelectedItem().toString()+"')");
            JOptionPane.showMessageDialog(null, "data berhasil ditambahkan");
        }
        catch(SQLException e) {
            JOptionPane.showMessageDialog(null,"Operasi Gagal");
        }
        karyawan();
        ko_nama.setText("");ko_password.setText("");
         }
    }//GEN-LAST:event_btn_simpanActionPerformed

    private void btn_hapusActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_hapusActionPerformed
        // TODO add your handling code here:
        try{ 
        stm.executeUpdate("DELETE FROM admin WHERE user='"+ko_nama.getText()+"' && pass='"+ko_password.getText()+"' && posisi='"+ko_status.getSelectedItem().toString()+"'");
         JOptionPane.showMessageDialog(null, "hapus berhasil");
                }catch(SQLException e) { 
            JOptionPane.showMessageDialog(null,"Operasi Gagal");
        } 
        karyawan();
        ko_nama.setText("");ko_password.setText("");      
    }//GEN-LAST:event_btn_hapusActionPerformed

    private void tabel_karyawan_produksiMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tabel_karyawan_produksiMouseClicked
        // TODO add your handling code here:
        field1();
    }//GEN-LAST:event_tabel_karyawan_produksiMouseClicked

    private void edit_karyawan_produksiActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_edit_karyawan_produksiActionPerformed
        // TODO add your handling code here:
         if(id_karyawan.getText().equalsIgnoreCase("") || nama_karyawan.getText().equalsIgnoreCase("") || gaji_karyawan.getText().equalsIgnoreCase("")){
        JOptionPane.showMessageDialog(null,"Maaf lengkapi data Terlebih dahulu");
        }else{
        
        
       try{ 
        stm.executeUpdate("update karyawan set gaji_karyawan='"+gaji_karyawan.getText()+"' where id_karyawan='"+id_karyawan.getText()+"'");
         JOptionPane.showMessageDialog(null, "edit berhasil");
                }catch(SQLException e) { 
            JOptionPane.showMessageDialog(null,"Operasi Gagal");
        } 
        karyawan_produksi();
        
        id_karyawan.setText("");
        nama_karyawan.setText("");
        gaji_karyawan.setText("");
         }
    }//GEN-LAST:event_edit_karyawan_produksiActionPerformed

    private void hapusActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_hapusActionPerformed
        // TODO add your handling code here:
       try{ 
        stm.executeUpdate("DELETE FROM karyawan WHERE id_karyawan='"+id_karyawan.getText()+"'");
         JOptionPane.showMessageDialog(null, "hapus berhasil");
                }catch(SQLException e) { 
            JOptionPane.showMessageDialog(null,"Operasi Gagal");
        } 
       karyawan_produksi();
        
        id_karyawan.setText("");
        nama_karyawan.setText("");
        gaji_karyawan.setText("");
    }//GEN-LAST:event_hapusActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(admin_karyawan.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(admin_karyawan.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(admin_karyawan.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(admin_karyawan.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new admin_karyawan().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private source_gambar.bgadmin bgadmin1;
    private javax.swing.JButton btn_hapus;
    private javax.swing.JButton btn_simpan;
    private javax.swing.JButton edit_karyawan_produksi;
    private javax.swing.JTextField gaji_karyawan;
    private javax.swing.JButton hapus;
    private javax.swing.JButton home;
    private javax.swing.JTextField id_karyawan;
    private javax.swing.JButton input;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JButton karyawan;
    private javax.swing.JTextField ko_nama;
    private javax.swing.JTextField ko_password;
    private javax.swing.JComboBox ko_status;
    private javax.swing.JButton lain;
    private javax.swing.JButton laporan;
    private javax.swing.JButton logout;
    private javax.swing.JTextField nama_karyawan;
    private javax.swing.JButton produk;
    private javax.swing.JButton stok;
    private javax.swing.JTable tabel_karyawan;
    private javax.swing.JTable tabel_karyawan_produksi;
    // End of variables declaration//GEN-END:variables
}
