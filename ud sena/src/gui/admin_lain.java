/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author SENA
 */
public class admin_lain extends javax.swing.JFrame {
Connection koneksi;
ResultSet RsBrg;
Statement stm;
String combobarang;
String tanggal;
private Object[][] dataTable = null;
private String[] header = {"ID","nama","`jumlah","fungsi","perawatan"};

    public admin_lain() {
        initComponents();
        posisi();
        buka_db();
        baca_data();
    }
    
void posisi(){
Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
Dimension frameSize = getSize();
setLocation(
(screenSize.width - frameSize.width) / 2,
(screenSize.height - frameSize.height) / 2);
    }

private void buka_db(){
    try{
    Class.forName("com.mysql.jdbc.Driver");
    koneksi = (Connection) DriverManager.getConnection("jdbc:mysql://localhost/ud_sena","root","");
    System.out.println("konek");
    }catch(ClassNotFoundException e){
    System.out.println("Eror #1(gak konek) : "+ e.getMessage());
    System.exit(0);
    }catch(SQLException e){
    System.out.println("Eror #2(gak konek) : "+ e.getMessage());
    System.exit(0);
}
} 

private void baca_data(){    
try{

stm = koneksi.createStatement();
//WHERE (tanggal BETWEEN '2007-12-01' AND '2008-01-01')
RsBrg = stm.executeQuery("SELECT * FROM mesin");

ResultSetMetaData meta = RsBrg.getMetaData();
int col = meta.getColumnCount();
int baris = 0;
while(RsBrg.next()) {
baris = RsBrg.getRow();
}
dataTable = new Object[baris][col];
int x = 0;
RsBrg.beforeFirst();
while(RsBrg.next()) {
dataTable[x][0] = RsBrg.getString("id_mesin");
dataTable[x][1] = RsBrg.getString("nama_mesin");
dataTable[x][2] = RsBrg.getString("jumlah_mesin");
dataTable[x][3] = RsBrg.getString("fungsi_mesin");
dataTable[x][4] = RsBrg.getString("perawatan_mesin");
x++;
    
}
mesin_tabel.setModel(new DefaultTableModel(dataTable,header));
}catch(SQLException e){
JOptionPane.showMessageDialog(null, "gagal memuat database");

    }

}  

public void field(){
    int row=mesin_tabel.getSelectedRow();
    in_id.setText((String)mesin_tabel.getValueAt(row,0));
    in_nama.setText((String)mesin_tabel.getValueAt(row,1));
    in_jumlah.setText((String)mesin_tabel.getValueAt(row,2));
    in_fungsi.setText((String)mesin_tabel.getValueAt(row,3));
    in_perawatan.setText((String)mesin_tabel.getValueAt(row,4));
}
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        bgadmin1 = new source_gambar.bgadmin();
        home = new javax.swing.JButton();
        input = new javax.swing.JButton();
        produk = new javax.swing.JButton();
        stok = new javax.swing.JButton();
        karyawan = new javax.swing.JButton();
        laporan = new javax.swing.JButton();
        lain = new javax.swing.JButton();
        logout = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        mesin_tabel = new javax.swing.JTable();
        in_nama = new javax.swing.JTextField();
        in_jumlah = new javax.swing.JTextField();
        in_fungsi = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        in_id = new javax.swing.JTextField();
        hapus = new javax.swing.JButton();
        simpan = new javax.swing.JButton();
        jLabel6 = new javax.swing.JLabel();
        in_perawatan = new javax.swing.JTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setUndecorated(true);

        bgadmin1.setPreferredSize(new java.awt.Dimension(1036, 600));

        home.setIcon(new javax.swing.ImageIcon(getClass().getResource("/gambar/btnhome.png"))); // NOI18N
        home.setText(" ");
        home.setPreferredSize(new java.awt.Dimension(260, 60));
        home.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                homeActionPerformed(evt);
            }
        });

        input.setIcon(new javax.swing.ImageIcon(getClass().getResource("/gambar/btninput.png"))); // NOI18N
        input.setText(" ");
        input.setPreferredSize(new java.awt.Dimension(260, 60));
        input.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                inputActionPerformed(evt);
            }
        });

        produk.setIcon(new javax.swing.ImageIcon(getClass().getResource("/gambar/btnproduk.png"))); // NOI18N
        produk.setText(" ");
        produk.setPreferredSize(new java.awt.Dimension(260, 60));
        produk.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                produkActionPerformed(evt);
            }
        });

        stok.setIcon(new javax.swing.ImageIcon(getClass().getResource("/gambar/btnstok.png"))); // NOI18N
        stok.setText(" ");
        stok.setPreferredSize(new java.awt.Dimension(260, 60));
        stok.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                stokActionPerformed(evt);
            }
        });

        karyawan.setIcon(new javax.swing.ImageIcon(getClass().getResource("/gambar/btnkaryawan.png"))); // NOI18N
        karyawan.setText(" ");
        karyawan.setPreferredSize(new java.awt.Dimension(260, 60));
        karyawan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                karyawanActionPerformed(evt);
            }
        });

        laporan.setIcon(new javax.swing.ImageIcon(getClass().getResource("/gambar/btnlaporan.png"))); // NOI18N
        laporan.setText(" ");
        laporan.setPreferredSize(new java.awt.Dimension(260, 60));
        laporan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                laporanActionPerformed(evt);
            }
        });

        lain.setIcon(new javax.swing.ImageIcon(getClass().getResource("/gambar/btnalat1.png"))); // NOI18N
        lain.setText(" ");
        lain.setPreferredSize(new java.awt.Dimension(260, 60));

        logout.setIcon(new javax.swing.ImageIcon(getClass().getResource("/gambar/btnlogout.png"))); // NOI18N
        logout.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                logoutActionPerformed(evt);
            }
        });

        mesin_tabel.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        mesin_tabel.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                mesin_tabelMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(mesin_tabel);

        jLabel4.setText("Fungsi ");

        jLabel3.setText("Jumlah ");

        jLabel2.setText("Nama");

        jLabel1.setText("ID ");

        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(0, 204, 204));
        jLabel5.setText("Biaya Perawatan");

        hapus.setText("Hapus");
        hapus.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                hapusActionPerformed(evt);
            }
        });

        simpan.setText("Simpan");
        simpan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                simpanActionPerformed(evt);
            }
        });

        jLabel6.setText("Biaya Perawatan");

        javax.swing.GroupLayout bgadmin1Layout = new javax.swing.GroupLayout(bgadmin1);
        bgadmin1.setLayout(bgadmin1Layout);
        bgadmin1Layout.setHorizontalGroup(
            bgadmin1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(bgadmin1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(bgadmin1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(bgadmin1Layout.createSequentialGroup()
                        .addComponent(home, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(271, 271, 271)
                        .addComponent(jLabel5)
                        .addContainerGap(274, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, bgadmin1Layout.createSequentialGroup()
                        .addGroup(bgadmin1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(bgadmin1Layout.createSequentialGroup()
                                .addComponent(lain, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(simpan)
                                .addGap(18, 18, 18)
                                .addComponent(hapus))
                            .addGroup(bgadmin1Layout.createSequentialGroup()
                                .addGroup(bgadmin1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(input, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(produk, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(stok, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(laporan, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(karyawan, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGroup(bgadmin1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 510, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(bgadmin1Layout.createSequentialGroup()
                                        .addGroup(bgadmin1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jLabel2)
                                            .addComponent(jLabel3)
                                            .addComponent(jLabel4)
                                            .addComponent(jLabel1)
                                            .addComponent(jLabel6))
                                        .addGap(23, 23, 23)
                                        .addGroup(bgadmin1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(in_fungsi)
                                            .addGroup(bgadmin1Layout.createSequentialGroup()
                                                .addGroup(bgadmin1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                    .addComponent(in_nama, javax.swing.GroupLayout.PREFERRED_SIZE, 220, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                    .addComponent(in_jumlah, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                    .addComponent(in_id, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                    .addComponent(in_perawatan, javax.swing.GroupLayout.PREFERRED_SIZE, 218, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                .addGap(0, 0, Short.MAX_VALUE)))))))
                        .addGap(105, 105, 105))))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, bgadmin1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(logout, javax.swing.GroupLayout.PREFERRED_SIZE, 86, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(19, 19, 19))
        );
        bgadmin1Layout.setVerticalGroup(
            bgadmin1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(bgadmin1Layout.createSequentialGroup()
                .addGap(24, 24, 24)
                .addComponent(logout)
                .addGroup(bgadmin1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(bgadmin1Layout.createSequentialGroup()
                        .addGap(36, 36, 36)
                        .addGroup(bgadmin1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(home, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel5))
                        .addGap(18, 18, 18)
                        .addGroup(bgadmin1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(bgadmin1Layout.createSequentialGroup()
                                .addComponent(input, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(produk, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addComponent(stok, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGroup(bgadmin1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(bgadmin1Layout.createSequentialGroup()
                                .addGap(18, 18, 18)
                                .addComponent(karyawan, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(laporan, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addGroup(bgadmin1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(lain, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(hapus)
                                    .addComponent(simpan)))
                            .addGroup(bgadmin1Layout.createSequentialGroup()
                                .addGap(35, 35, 35)
                                .addGroup(bgadmin1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jLabel3)
                                    .addComponent(in_jumlah, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(18, 18, 18)
                                .addGroup(bgadmin1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel4)
                                    .addComponent(in_fungsi, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(18, 18, 18)
                                .addGroup(bgadmin1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel6)
                                    .addComponent(in_perawatan, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                        .addContainerGap(27, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, bgadmin1Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(bgadmin1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(in_id, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel1))
                        .addGap(18, 18, 18)
                        .addGroup(bgadmin1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel2)
                            .addComponent(in_nama, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(196, 196, 196))))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(bgadmin1, javax.swing.GroupLayout.DEFAULT_SIZE, 955, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(bgadmin1, javax.swing.GroupLayout.PREFERRED_SIZE, 540, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void logoutActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_logoutActionPerformed
        // TODO add your handling code here:
                    login go = new login();
                    go.setVisible(true);
                    dispose();
    }//GEN-LAST:event_logoutActionPerformed

    private void homeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_homeActionPerformed
        // TODO add your handling code here:
                    admin go = new admin();
                    go.setVisible(true);
                    dispose();
    }//GEN-LAST:event_homeActionPerformed

    private void inputActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_inputActionPerformed
        // TODO add your handling code here:
                    admin_input go = new admin_input();
                    go.setVisible(true);
                    dispose();
    }//GEN-LAST:event_inputActionPerformed

    private void produkActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_produkActionPerformed
        // TODO add your handling code here:
                    admin_produk go = new admin_produk();
                    go.setVisible(true);
                    dispose();
    }//GEN-LAST:event_produkActionPerformed

    private void stokActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_stokActionPerformed
        // TODO add your handling code here:
                    admin_stok go = new admin_stok();
                    go.setVisible(true);
                    dispose();
    }//GEN-LAST:event_stokActionPerformed

    private void karyawanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_karyawanActionPerformed
        // TODO add your handling code here:
                    admin_karyawan go = new admin_karyawan();
                    go.setVisible(true);
                    dispose();
    }//GEN-LAST:event_karyawanActionPerformed

    private void laporanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_laporanActionPerformed
        // TODO add your handling code here:
                    admin_laporan go = new admin_laporan();
                    go.setVisible(true);
                    dispose();
    }//GEN-LAST:event_laporanActionPerformed

    private void simpanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_simpanActionPerformed
        // TODO add your handling code here:
                if(in_id.getText().equalsIgnoreCase("") || in_nama.getText().equalsIgnoreCase("") || in_jumlah.getText().equalsIgnoreCase("") || in_fungsi.getText().equalsIgnoreCase("")|| in_perawatan.getText().equalsIgnoreCase("")){
        JOptionPane.showMessageDialog(null,"Maaf lengkapi data Terlebih dahulu");
        }else{
        
        
         try{
            stm = koneksi.createStatement();
            stm.executeUpdate("insert into mesin value ('"+in_id.getText()+"','"+in_nama.getText()+"','"+in_jumlah.getText()+"','"+in_fungsi.getText()+"','"+in_perawatan.getText()+"')");
            JOptionPane.showMessageDialog(null, "data berhasil ditambahkan");
        }
        catch(SQLException e) {
            JOptionPane.showMessageDialog(null,"Operasi Gagal");
        }
         baca_data();
         clear();}
    }//GEN-LAST:event_simpanActionPerformed

    private void mesin_tabelMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_mesin_tabelMouseClicked
        // TODO add your handling code here:
        field();
    }//GEN-LAST:event_mesin_tabelMouseClicked

    private void hapusActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_hapusActionPerformed
        // TODO add your handling code here:
        try{
            String sql="delete from mesin where id_mesin='"+in_id.getText()+"'";
            stm.executeUpdate(sql);
            JOptionPane.showMessageDialog(null, "delete berhasil");
        }catch(SQLException e){
            JOptionPane.showMessageDialog(null, "delete gagal");
        }
        baca_data();
        clear();
    }//GEN-LAST:event_hapusActionPerformed

    void clear(){
    in_id.setText("");
    in_nama.setText("");
    in_jumlah.setText("");
    in_fungsi.setText("");
    in_perawatan.setText("");
    }
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(admin_lain.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(admin_lain.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(admin_lain.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(admin_lain.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new admin_lain().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private source_gambar.bgadmin bgadmin1;
    private javax.swing.JButton hapus;
    private javax.swing.JButton home;
    private javax.swing.JTextField in_fungsi;
    private javax.swing.JTextField in_id;
    private javax.swing.JTextField in_jumlah;
    private javax.swing.JTextField in_nama;
    private javax.swing.JTextField in_perawatan;
    private javax.swing.JButton input;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JButton karyawan;
    private javax.swing.JButton lain;
    private javax.swing.JButton laporan;
    private javax.swing.JButton logout;
    private javax.swing.JTable mesin_tabel;
    private javax.swing.JButton produk;
    private javax.swing.JButton simpan;
    private javax.swing.JButton stok;
    // End of variables declaration//GEN-END:variables
}
