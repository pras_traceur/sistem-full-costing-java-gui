/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author SENA
 */
public class operator_input extends javax.swing.JFrame {
Connection koneksi;
ResultSet RsBrg;
Statement stm;
String combobarang;
String tanggal;
private Object[][] dataTable = null;
private String[] header = {"no","bahan","jumlah","satuan"};
//hitung//
    String jumlahstok []=new String[20];
    String totalstok []=new String[20];

    
    int   jumlahz[]=new int[20];
    ///////////////////////
    String totalgaji,jenis,aa,bb,cc,dd,ee,ff,oo,pp;
    //////////////////////
    String jumlahbahan []=new String[20];
    String bahan []=new String[20];
    String jumlahgaji []=new String[20];
    String gaji []=new String[20];
    String jumlahuntung []=new String[20];
    int z=0,a=0,b=0,c=0,jumlah1=0,jumlah2=0,jumlaha,jumlahb,jumlahc = 0;
    int jumlah,persen,total;
    
    public operator_input() {
        initComponents();
        posisi();
        buka_db();
        inc();
        tampilcombo();
        tampilcombo1();
        in_nopesanan.setEditable(false);
        in_max.setEditable(false);
        
        
    }
void inc(){
    try {
            Statement statement = (Statement) conek.GetConnection().createStatement();
            ResultSet result=statement.executeQuery("select * from proses order by no desc limit 0,1");

            if (result.next()) {

                    String tampung = result.getString("no");
                    in_nopesanan.setText((Integer.parseInt(tampung)+1)+"");
                 
                }   
                } catch (Exception e) {
            
        }
                      
        
                if (in_nopesanan.getText().equals("")) {
                     
                    in_nopesanan.setText(1+"");
                    
                }   
            
 }

void posisi(){
    Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
    Dimension frameSize = getSize();
    setLocation(
    (screenSize.width - frameSize.width) / 2,
    (screenSize.height - frameSize.height) / 2);
}
    
private void buka_db(){
    try{
    Class.forName("com.mysql.jdbc.Driver");
    koneksi = (Connection) DriverManager.getConnection("jdbc:mysql://localhost/ud_sena","root","");

    }catch(ClassNotFoundException e){

    System.exit(0);
    }catch(SQLException e){
    
    System.exit(0);
}
}    
    
public void tampilcombo(){
try {

    String sql="SELECT * FROM barang_produksi";//tampilkan tb_jurusan
    Statement stat = (Statement) koneksi.createStatement();
    ResultSet res = stat.executeQuery(sql);
    while (res.next()){
//Untuk menampilkan Data dari dalam Table di database kedalam ComboBo
    in_combo.addItem(res.getString("nama_barang"));

}
} catch (Exception e) {
    JOptionPane.showMessageDialog(null,"Terjadi Kesalahan" +e);
}
}      

public void tampilcombo1(){
try {

    String sql="SELECT * FROM karyawan";//tampilkan tb_jurusan
    Statement stat = (Statement) koneksi.createStatement();
    ResultSet res = stat.executeQuery(sql);
    while (res.next()){
//Untuk menampilkan Data dari dalam Table di database kedalam ComboBo
    in_tukang.addItem(res.getString("tim_karyawan"));

}
} catch (Exception e) {
    JOptionPane.showMessageDialog(null,"Terjadi Kesalahan" +e);
}
}

private void baca_data(){    
try{
String input = in_combo.getSelectedItem().toString();
stm = koneksi.createStatement();
//WHERE (tanggal BETWEEN '2007-12-01' AND '2008-01-01')
RsBrg = stm.executeQuery("SELECT * FROM bahan where bahan.nama_barang='"+input+"'");

ResultSetMetaData meta = RsBrg.getMetaData();
int col = meta.getColumnCount();
int baris = 0;
while(RsBrg.next()) {
baris = RsBrg.getRow();
}
dataTable = new Object[baris][col];
int x = 0;
RsBrg.beforeFirst();
while(RsBrg.next()) {
dataTable[x][0] = x+1;
dataTable[x][1] = RsBrg.getString("jenis");

dataTable[x][2] = RsBrg.getString("jumlah");

dataTable[x][3] = RsBrg.getString("satuan");
x++;
    
}
in_tabel.setModel(new DefaultTableModel(dataTable,header));
}catch(SQLException e){
JOptionPane.showMessageDialog(null, "gagal memuat database");

    }

}    
    
void hitung(){
    String input = in_combo.getSelectedItem().toString();
  
try {
    String sqli="SELECT * FROM bahan where nama_barang='"+input+"' && jenis='kayu';";
    Statement stati = (Statement) koneksi.createStatement();
    ResultSet resi = stati.executeQuery(sqli);
    while (resi.next()){
            jumlahstok[1]=resi.getString("jumlah");
    }

    String sql="SELECT harga,jumlah,SUM(jumlah) as jumlahakhir FROM stok WHERE jenis ='kayu';";
    Statement stat = (Statement) koneksi.createStatement();
    ResultSet res = stat.executeQuery(sql);
    while (res.next()){
            totalstok[1]=res.getString("jumlahakhir");

    }
} catch (Exception e) {}   

    in_max.setText(Integer.parseInt(totalstok[1])/Integer.parseInt(jumlahstok[1])+"");
      

    String in_gaji = in_tukang.getSelectedItem().toString();
    try {

    String sql="SELECT gaji_karyawan FROM karyawan WHERE tim_karyawan='"+in_gaji+"';";//tampilkan tb_jurusan
    Statement stat = (Statement) koneksi.createStatement();
    ResultSet res = stat.executeQuery(sql);
    while (res.next()){
            totalgaji=res.getString("gaji_karyawan");
    }
} catch (Exception e) {}
    
    
    
    try {

    String sql="SELECT bahan.jumlah,harga FROM stok,bahan WHERE stok.jenis = bahan.jenis && bahan.nama_barang='"+input+"';";//tampilkan tb_jurusan
    Statement stat = (Statement) koneksi.createStatement();
    ResultSet res = stat.executeQuery(sql);
    while (res.next()){
            jumlahbahan[a]=res.getString("bahan.jumlah");
            bahan[a]=res.getString("harga");
            jumlaha=Integer.parseInt(jumlahbahan[a])*Integer.parseInt(bahan[a]);
            a++;   
            jumlah1 +=jumlaha;
    }
} catch (Exception e) {}

try {

    String sql="SELECT jumlah_mesin,perawatan_mesin FROM mesin;";//tampilkan tb_jurusan
    Statement stat = (Statement) koneksi.createStatement();
    ResultSet res = stat.executeQuery(sql);
    while (res.next()){
            jumlahgaji[b]=res.getString("jumlah_mesin");
            gaji[b]=res.getString("perawatan_mesin");
            jumlahb=Integer.parseInt(jumlahgaji[b])*Integer.parseInt(gaji[b]);

            b++;   
            jumlah2 +=jumlahb;
    }
} catch (Exception e) {}

  
try {

    String sql="SELECT persentase_untung FROM barang_produksi WHERE nama_barang='"+input+"';";//tampilkan tb_jurusan
    Statement stat = (Statement) koneksi.createStatement();
    ResultSet res = stat.executeQuery(sql);
    while (res.next()){
            jumlahuntung[c]=res.getString("persentase_untung");
            jumlahc=Integer.parseInt(jumlahuntung[c]);
            c++;        
    }
} catch (Exception e) {}

    /////////////////////////////////
    ////////////RUMUS///////////////
     
     persen = (jumlah1/100)*jumlahc;
     total = jumlah1+persen;
     jumlah = total +jumlah2+Integer.parseInt(totalgaji);
     in_hargatotal.setText(jumlah+"");
    ////////////RUMUS///////////////
    /////////////////////////////////
    
}



    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        bgoperator1 = new source_gambar.bgoperator();
        home = new javax.swing.JButton();
        input = new javax.swing.JButton();
        laporan = new javax.swing.JButton();
        logout = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        in_tabel = new javax.swing.JTable();
        jLabel2 = new javax.swing.JLabel();
        in_combo = new javax.swing.JComboBox();
        in_nopesanan = new javax.swing.JTextField();
        in_lihat = new javax.swing.JButton();
        in_jumlah = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        in_tgl = new com.toedter.calendar.JDateChooser();
        jLabel16 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        btn_lihat = new javax.swing.JButton();
        jLabel14 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        in_hargatotal = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        in_pemesan = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        harga_total = new javax.swing.JTextField();
        in_hp = new javax.swing.JTextField();
        tombol_input = new javax.swing.JButton();
        jLabel3 = new javax.swing.JLabel();
        in_tukang = new javax.swing.JComboBox();
        jLabel4 = new javax.swing.JLabel();
        in_max = new javax.swing.JTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setUndecorated(true);

        bgoperator1.setPreferredSize(new java.awt.Dimension(955, 540));

        home.setIcon(new javax.swing.ImageIcon(getClass().getResource("/gambar/btnhome.png"))); // NOI18N
        home.setText(" ");
        home.setPreferredSize(new java.awt.Dimension(260, 60));
        home.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                homeActionPerformed(evt);
            }
        });

        input.setIcon(new javax.swing.ImageIcon(getClass().getResource("/gambar/btninput1.png"))); // NOI18N
        input.setText(" ");
        input.setPreferredSize(new java.awt.Dimension(260, 60));
        input.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                inputActionPerformed(evt);
            }
        });

        laporan.setIcon(new javax.swing.ImageIcon(getClass().getResource("/gambar/btnlaporan.png"))); // NOI18N
        laporan.setText(" ");
        laporan.setPreferredSize(new java.awt.Dimension(260, 60));
        laporan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                laporanActionPerformed(evt);
            }
        });

        logout.setIcon(new javax.swing.ImageIcon(getClass().getResource("/gambar/btnlogout.png"))); // NOI18N
        logout.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                logoutActionPerformed(evt);
            }
        });

        in_tabel.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "no", "bahan", "jumlah", "satuan"
            }
        ));
        jScrollPane1.setViewportView(in_tabel);

        jLabel2.setText("Jumlah");

        in_combo.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "-------------------" }));

        in_lihat.setText("Lihat");
        in_lihat.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                in_lihatActionPerformed(evt);
            }
        });

        jLabel8.setText("Per PRODUKSI");

        in_tgl.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                in_tglPropertyChange(evt);
            }
        });

        jLabel16.setText("TOTAL");

        jLabel15.setText("TGL Pesan");

        btn_lihat.setText("lihat");
        btn_lihat.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_lihatActionPerformed(evt);
            }
        });

        jLabel14.setText("Pemesan");

        jLabel13.setText("NO HP");

        jLabel5.setText("Pesanan");

        jLabel6.setText("item");

        jLabel1.setText("No Pesanan");

        jLabel9.setText("Rp");

        tombol_input.setText("INPUT");
        tombol_input.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tombol_inputActionPerformed(evt);
            }
        });

        jLabel3.setText("Tukang");

        in_tukang.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "------------------" }));

        jLabel4.setText("Maksimal Produksi");

        javax.swing.GroupLayout bgoperator1Layout = new javax.swing.GroupLayout(bgoperator1);
        bgoperator1.setLayout(bgoperator1Layout);
        bgoperator1Layout.setHorizontalGroup(
            bgoperator1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(bgoperator1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(bgoperator1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(home, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(input, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(laporan, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(64, 64, 64)
                .addGroup(bgoperator1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(bgoperator1Layout.createSequentialGroup()
                        .addGroup(bgoperator1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel2)
                            .addComponent(jLabel16))
                        .addGap(42, 42, 42)
                        .addGroup(bgoperator1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(harga_total, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(tombol_input, javax.swing.GroupLayout.PREFERRED_SIZE, 127, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(bgoperator1Layout.createSequentialGroup()
                                .addComponent(in_jumlah, javax.swing.GroupLayout.PREFERRED_SIZE, 121, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jLabel6)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(btn_lihat, javax.swing.GroupLayout.PREFERRED_SIZE, 55, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addGroup(bgoperator1Layout.createSequentialGroup()
                        .addGroup(bgoperator1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel1)
                            .addComponent(jLabel14)
                            .addComponent(jLabel13)
                            .addComponent(jLabel15))
                        .addGap(18, 18, 18)
                        .addGroup(bgoperator1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(in_hp, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 141, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(in_pemesan, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 141, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(in_nopesanan, javax.swing.GroupLayout.PREFERRED_SIZE, 141, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(in_tgl, javax.swing.GroupLayout.PREFERRED_SIZE, 141, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 55, Short.MAX_VALUE)
                .addGroup(bgoperator1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 300, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(bgoperator1Layout.createSequentialGroup()
                        .addComponent(jLabel8)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel9)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(in_hargatotal, javax.swing.GroupLayout.PREFERRED_SIZE, 174, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(bgoperator1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addGroup(bgoperator1Layout.createSequentialGroup()
                            .addComponent(jLabel4)
                            .addGap(18, 18, 18)
                            .addComponent(in_max))
                        .addGroup(javax.swing.GroupLayout.Alignment.LEADING, bgoperator1Layout.createSequentialGroup()
                            .addComponent(jLabel5)
                            .addGap(18, 18, 18)
                            .addGroup(bgoperator1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(in_tukang, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(in_combo, javax.swing.GroupLayout.PREFERRED_SIZE, 114, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGap(33, 33, 33)
                            .addComponent(in_lihat)))
                    .addComponent(jLabel3))
                .addGap(27, 27, 27))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, bgoperator1Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(logout, javax.swing.GroupLayout.PREFERRED_SIZE, 86, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(19, 19, 19))
        );
        bgoperator1Layout.setVerticalGroup(
            bgoperator1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(bgoperator1Layout.createSequentialGroup()
                .addGap(24, 24, 24)
                .addComponent(logout)
                .addGroup(bgoperator1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(bgoperator1Layout.createSequentialGroup()
                        .addGap(78, 78, 78)
                        .addComponent(home, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(input, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(laporan, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(27, 233, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, bgoperator1Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(bgoperator1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(bgoperator1Layout.createSequentialGroup()
                                .addGroup(bgoperator1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel3)
                                    .addComponent(in_tukang, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(bgoperator1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(bgoperator1Layout.createSequentialGroup()
                                        .addGap(3, 3, 3)
                                        .addGroup(bgoperator1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                            .addComponent(jLabel5)
                                            .addComponent(in_combo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                    .addComponent(in_lihat, javax.swing.GroupLayout.Alignment.TRAILING))
                                .addGap(21, 21, 21)
                                .addGroup(bgoperator1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel4)
                                    .addComponent(in_max, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(33, 33, 33)
                                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 128, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(32, 32, 32)
                                .addGroup(bgoperator1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel8)
                                    .addComponent(in_hargatotal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel9)))
                            .addGroup(bgoperator1Layout.createSequentialGroup()
                                .addGap(6, 6, 6)
                                .addGroup(bgoperator1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel1)
                                    .addComponent(in_nopesanan, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(6, 6, 6)
                                .addGroup(bgoperator1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel14)
                                    .addComponent(in_pemesan, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(9, 9, 9)
                                .addGroup(bgoperator1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel13)
                                    .addComponent(in_hp, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(18, 18, 18)
                                .addGroup(bgoperator1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(in_tgl, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel15))
                                .addGap(51, 51, 51)
                                .addGroup(bgoperator1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(in_jumlah, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel6)
                                    .addComponent(btn_lihat)
                                    .addComponent(jLabel2))
                                .addGap(38, 38, 38)
                                .addGroup(bgoperator1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel16)
                                    .addComponent(harga_total, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(27, 27, 27)
                                .addComponent(tombol_input, javax.swing.GroupLayout.PREFERRED_SIZE, 49, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(49, 49, 49))))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(bgoperator1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(bgoperator1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void logoutActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_logoutActionPerformed
        // TODO add your handling code here:
        login go = new login();
        go.setVisible(true);
        dispose();
    }//GEN-LAST:event_logoutActionPerformed

    private void inputActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_inputActionPerformed
        // TODO add your handling code here:
                    operator_input go = new operator_input();
                    go.setVisible(true);
                    dispose();
    }//GEN-LAST:event_inputActionPerformed

    private void laporanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_laporanActionPerformed
        // TODO add your handling code here:
                    operator_laporan go = new operator_laporan();
                    go.setVisible(true);
                    dispose();
    }//GEN-LAST:event_laporanActionPerformed

    private void homeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_homeActionPerformed
        // TODO add your handling code here:
                    operator go = new operator();
                    go.setVisible(true);
                    dispose();
    }//GEN-LAST:event_homeActionPerformed

    private void in_lihatActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_in_lihatActionPerformed
        // TODO add your handling code here:
        baca_data();

        hitung();
    }//GEN-LAST:event_in_lihatActionPerformed

    private void in_tglPropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_in_tglPropertyChange
        // TODO add your handling code here:
        if (in_tgl.getDate()!=null){
            SimpleDateFormat Format=new SimpleDateFormat("yyyy-MM-dd");
            tanggal=Format.format(in_tgl.getDate());}
    }//GEN-LAST:event_in_tglPropertyChange

    private void btn_lihatActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_lihatActionPerformed
        // TODO add your handling code here:
        String a = in_hargatotal.getText();
        String b = in_jumlah.getText();
        ////////////RUMUS///////////////
        ////////////////////////////////
        int total = Integer.parseInt(a)*Integer.parseInt(b);
        harga_total.setText(total+"");
    }//GEN-LAST:event_btn_lihatActionPerformed

    private void tombol_inputActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tombol_inputActionPerformed
if(in_nopesanan.getText().equalsIgnoreCase("")|| in_pemesan.getText().equalsIgnoreCase("")  || in_hp.getText().equalsIgnoreCase("")  || in_jumlah.getText().equalsIgnoreCase("")  || in_max.getText().equalsIgnoreCase("")){
        JOptionPane.showMessageDialog(null,"Maaf lengkapi data Terlebih dahulu");
 
}else{
         if(Integer.parseInt(in_max.getText()) >= Integer.parseInt(in_jumlah.getText())){
        String input = in_combo.getSelectedItem().toString();
        int untungseluruh = persen*Integer.parseInt(in_jumlah.getText());
        int perawat=jumlah2*Integer.parseInt(in_jumlah.getText());
        try{
            stm.executeUpdate("INSERT into proses VALUES('"+in_nopesanan.getText()+"','"+in_pemesan.getText()+"','"+in_hp.getText()+"','"+tanggal+"','"+in_tukang.getSelectedItem().toString()+"','"+input+"','"+jumlah+"','"+jumlahc+"','"+persen+"','"+in_jumlah.getText()+"','"+untungseluruh+"','"+total+"','"+harga_total.getText() +"','belum','"+perawat+"');");
            
        }catch(SQLException e) {
           
        }
        
        
        int gajikaryawan = Integer.parseInt(totalgaji)*Integer.parseInt(in_jumlah.getText());
        try{
            stm.executeUpdate("INSERT into gaji (tim_karyawan,jumlah,barang_proses,tgl_proses,gaji_karyawan,status_gaji) VALUES('"+in_tukang.getSelectedItem().toString()+"','"+in_jumlah.getText()+"','"+input+"','"+tanggal+"','"+gajikaryawan+"','belum');");
           
        }catch(SQLException e) {
            
        }
        
        try{
            stm.executeUpdate("INSERT into perawatan_mesin (jumlah,barang_proses,tgl_proses,biaya_perawatan,status_biayaperawatan) VALUES('"+in_jumlah.getText()+"','"+input+"','"+tanggal+"','"+perawat+"','belum');");
            JOptionPane.showMessageDialog(null,"Operasi Berhasil");
        }catch(SQLException e) {
            JOptionPane.showMessageDialog(null,"Operasi Gagal");
        }
///////////////////////////////STOK///////////////////////////////////        
try {

    
    String sql="SELECT * FROM bahan where nama_barang='"+input+"';";//
    Statement stat = (Statement) koneksi.createStatement();
    ResultSet res = stat.executeQuery(sql);
    while (res.next()){
   
            jenis=res.getString("jenis");  
            oo=res.getString("jumlah"); 
                String sqli="SELECT * FROM stok WHERE jenis='"+jenis+"';";//
                Statement stati = (Statement) koneksi.createStatement();
                ResultSet resi = stati.executeQuery(sqli);
                while (resi.next()){
                aa=resi.getString("jumlah");
                cc=resi.getString("no");
                
                }
            bb=res.getString("jumlah");
            int hasil = Integer.parseInt(bb)*Integer.parseInt(in_jumlah.getText());
            int hasil2 = Integer.parseInt(aa)-hasil;
            int hasil3 = Integer.parseInt(in_jumlah.getText())-(Integer.parseInt(aa)/Integer.parseInt(oo));
            System.out.println(hasil2);
            
            if(hasil2<=0){
            try {
                System.out.println(cc);
            String sqlii="delete from stok where no='"+cc+"'";
            stm.executeUpdate(sqlii);
            
            ///////////////////////////////////////////////////////////
            if(jenis.equalsIgnoreCase("kayu")){
try {
    String sqla="SELECT * FROM bahan where nama_barang='"+input+"';";//tampilkan tb_jurusan
    Statement stata = (Statement) koneksi.createStatement();
    ResultSet resa = stata.executeQuery(sqla);
    while (resa.next()){
        
            jenis=resa.getString("jenis");  
                String sqlb="SELECT * FROM stok WHERE jenis='"+jenis+"';";//tampilkan tb_jurusan
                Statement statb = (Statement) koneksi.createStatement();
                ResultSet resb = statb.executeQuery(sqlb);
                while (resb.next()){
                dd=resb.getString("jumlah");
                ff=resb.getString("no");
                }
            ee=resa.getString("jumlah");
            int hasila = Integer.parseInt(ee)*hasil3;
            int hasil2a = Integer.parseInt(dd)-hasila;
            
            System.out.println(hasil2a+" dua");
            if(hasil2a<=0){
            try {
             System.out.println(ff+" dua");
            String sqlc="delete from stok where no='"+ff+"'";
            stm.executeUpdate(sqlc);
           
                }catch(SQLException e){}
            }else{
            stm.executeUpdate("update stok set jumlah='"+hasil2a+"' where jenis='" +jenis + "'") ;

            }
    }
} catch (Exception e) {}
            }
           /////////////////////////////////////////////////////////////
                }catch(SQLException e){}
            }else{
            stm.executeUpdate("update stok set jumlah='"+hasil2+"' where jenis='" +jenis + "'") ;
            }
    }
} catch (Exception e) {}
        

        
        
        
        
        
        
        
        operator_input go = new operator_input();
        go.setVisible(true);
        dispose();
        }
else{
         JOptionPane.showMessageDialog(null,"Maaf Jumlah Pemesanan melebihi Stok yang ada");
        
         }
        }  
    }//GEN-LAST:event_tombol_inputActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(operator_input.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(operator_input.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(operator_input.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(operator_input.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new operator_input().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private source_gambar.bgoperator bgoperator1;
    private javax.swing.JButton btn_lihat;
    private javax.swing.JTextField harga_total;
    private javax.swing.JButton home;
    private javax.swing.JComboBox in_combo;
    private javax.swing.JTextField in_hargatotal;
    private javax.swing.JTextField in_hp;
    private javax.swing.JTextField in_jumlah;
    private javax.swing.JButton in_lihat;
    private javax.swing.JTextField in_max;
    private javax.swing.JTextField in_nopesanan;
    private javax.swing.JTextField in_pemesan;
    private javax.swing.JTable in_tabel;
    private com.toedter.calendar.JDateChooser in_tgl;
    private javax.swing.JComboBox in_tukang;
    private javax.swing.JButton input;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JButton laporan;
    private javax.swing.JButton logout;
    private javax.swing.JButton tombol_input;
    // End of variables declaration//GEN-END:variables
}
